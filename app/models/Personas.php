<?php


namespace models;


class Personas extends connection
{
    public $idP;
    public $idE;
    public $sexo;
    public $edad;
    public $semana;
    public $estatus;

    //Funcion para consultar registros de la bd
    public static function findAll(){
        //Preparamos la conexion instanciandola
        $co = new \models\connection();
        //preparamos la sentencia sql
        $pre = mysqli_prepare($co->con, "SELECT * FROM personas");
        //Ejecutamos el query
        $pre->execute();
        //Almacenamos el resultado obtenido del objeto
        $resul = $pre->get_result();
        return $resul;
    }

    public function eliminar($id){
        $pre = mysqli_prepare($this->con,"DELETE FROM `personas` WHERE `id_persona`=(?)");
        $pre->bind_param("i",  $id);
        $pre->execute();

    }
    //funcion insertar
    public function insertar(){
        //Prepara la sentencia sql
        $pre = mysqli_prepare($this->con,"INSERT INTO `personas`(`ID_Estado`, `Sexo`, `Edad`,`Semana`, `Estatus`) VALUES (?,?,?,?,?)");
        //Pasa parametros con las variables y sus tipos
        $pre->bind_param("isiii",  $this->idE, $this-> sexo, $this->edad,$this->semana, $this->estatus);
        //Ejecuta el query
        $pre->execute();
    }
    public static function allEdadPromedio($estado){
        //Preparamos la conexion instanciandola
        $co = new \models\connection();
        //preparamos la sentencia sql
        $resul=mysqli_query($co->con,"SELECT AVG(Edad) AS Edad_Promedio FROM personas WHERE ID_Estado =$estado");
        $row = mysqli_fetch_assoc($resul);
        $resul2=$row['Edad_Promedio'];
        return $resul2;
    }
    public static function sexoM($estado){
        //Preparamos la conexion instanciandola
        $co = new \models\connection();
        $resul=mysqli_query($co->con,"SELECT count(Sexo) FROM personas WHERE Sexo='M' AND ID_Estado=$estado");
        $row = mysqli_fetch_assoc($resul);
        $resul2=$row['count(Sexo)'];
        return $resul2;
    }
    public static function sexoF($estado){
        //Preparamos la conexion instanciandola
        $co = new \models\connection();
        $resul=mysqli_query($co->con,"SELECT count(Sexo) FROM personas WHERE Sexo='F' AND ID_Estado=$estado");
        $row = mysqli_fetch_assoc($resul);
        $resul2=$row['count(Sexo)'];
        return $resul2;
    }
    //Funcion para ver los contagiados de un estado
    public static function allEdadPromedioC($estado){
        //Preparamos la conexion instanciandola
        $co = new \models\connection();
        //preparamos la sentencia sql
        $resul=mysqli_query($co->con,"SELECT AVG(Edad) AS Edad_Promedio FROM personas WHERE Estatus=1 AND ID_Estado =$estado");
        $row = mysqli_fetch_assoc($resul);
        $resul2=$row['Edad_Promedio'];
        return $resul2;
    }
    //funcion para ver las personas contagiadas de sexo M
    public static function sexoContagiadoM($estado){
        //Preparamos la conexion instanciandola
        $co = new \models\connection();
        $resul=mysqli_query($co->con,"SELECT count(Sexo) FROM personas WHERE Sexo='M' AND Estatus=1 AND ID_Estado=$estado");
        $row = mysqli_fetch_assoc($resul);
        $resul2=$row['count(Sexo)'];
        return $resul2;
    }
    //funcion para ver las personas contagiadas de sexo F
    public static function sexoContagiadoF($estado){
        //Preparamos la conexion instanciandola
        $co = new \models\connection();
        $resul=mysqli_query($co->con,"SELECT count(Sexo) FROM personas WHERE Sexo='F' AND Estatus=1 AND ID_Estado=$estado");
        $row = mysqli_fetch_assoc($resul);
        $resul2=$row['count(Sexo)'];
        return $resul2;
    }


}

?>