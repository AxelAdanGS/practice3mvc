<?php


namespace models;


//Clase de estados pque realiza conexion
class Estados extends connection
{
    //Atriutos de la clase
    public $id;
    public $nombre;
    public $poblacion;

    //Funcion para consultar registros de la bd
    public static function findAll(){
        //Preparamos la conexion instanciandola
        $co = new \models\connection();
        //preparamos la sentencia sql
        $pre = mysqli_prepare($co->con, "SELECT * FROM estados");
        //Ejecutamos el query
        $pre->execute();
        //Almacenamos el resultado obtenido del objeto
        $resul = $pre->get_result();

        //Almacena en n arreglo los datos obtenidos
        while($elemento = mysqli_fetch_assoc($resul)){
            $elementos[] = $elemento;

        }

        return $elementos;
    }
}