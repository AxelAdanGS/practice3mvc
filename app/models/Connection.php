<?php
//Ya quedo


namespace models;


//Clase para conectar a la base de datos
class connection
{   
    //Variable publica para realizar conexion
    public $con;

    //Metodo constructor para ejecutar la conexion cuando se instancie
    function __construct()
    {
        $host = "localhost";//Var para el host
        $user = "root";//Variable para usuario
        $pass = "";//Variable para contraseña

        //Cambie el nombre de la bd
        $db = "contagios_estado";//Nombre de la bd
     
        //Variable para realizar conexion al servidor
        $this->con = mysqli_connect($host, $user, $pass, $db);

        //Permite el uso de acento
        mysqli_query($this->con, "SET NAMES 'utf8");
    }
}