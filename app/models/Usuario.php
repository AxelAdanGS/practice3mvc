<?php


namespace models;


class Usuario extends connection
{
//funcion insertar
    public function insertar(){
        //Prepara la sentencia sql
        $pre = mysqli_prepare($this->con,"INSERT INTO `usuario`(`nombre`, `apellidoP`, `apellidoM`, `sexo`,`edad`,`correo`,`contrasenia`) VALUES (?,?,?,?,?,?,?)");
        //Pasa parametros con las variables y sus tipos
        $pre->bind_param("ssssiss",  $this->nombre, $this->aP, $this->aM, $this->sexo, $this->edad, $this->correo, $this->pass);
        //Ejecuta el query
        $pre->execute();
    }
    static function logins($correo, $password){
        //Preparamos la conexion instanciandola
        $co = new \models\connection();

        //preparamos la sentencia sql
        $pre = mysqli_prepare($co->con, "SELECT * FROM usuario WHERE correo = ? AND contrasenia = ?");

        $pre->bind_param("ss", $correo, $password);
        //Ejecutamos el query
        $pre->execute();

        //Almacenamos el resultado obtenido del objeto
        $resul = $pre->get_result();

        return $resul->fetch_object();
    }
}