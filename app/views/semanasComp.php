<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>COVID-19 | Semanas </title>
    <link rel="icon" type="image/png" href="public/images/virus.png">
    <link rel="stylesheet" href="../../public/css/table.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark  bg-primary">
    <a class="navbar-brand" href="?controller=usuario&action=home">COVID-19</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href=?controller=estados&action=ver>Estado <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href=?controller=estados&action=verC>Contagios por estado<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href=?controller=semanas&action=compaSem1>Comparar dos semanas<span class="sr-only">(current)</span></a>
            </li>
            <?php session_start(); if (!isset($_SESSION['usuario'])){ ?>
                <li class="nav-item active">
                    <a class="nav-link" href="?controller=usuario&action=loginView">Iniciar Sesion<span class="sr-only">(current)</span></a>
                </li>
            <?php } ?>

            <?php if (isset($_SESSION['usuario'])){ ?>
                <li class="nav-item active">
                    <a class="nav-link" href=?controller=semanas&action=registrarSem>Registrar Semanas<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href=?controller=personas&action=imprimirPersonas>Personas<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href=?controller=personas&action=rePer>Registrar Persona<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href=?controller=usuario&action=ver>Agregar Usuario<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="?controller=usuario&action=logout">Cerrar sesion<span class="sr-only">(current)</span></a>
                </li>
            <?php } ?>

        </ul>
    </div>
</nav>
<div class="container p-4 fadeIn text-center">
    <div class="row">
        <h3 class="text-center">Semana 1</h3>

        <table class="table">
            <thead>
            <tr class="bg-primary">
                <th scope="col">No.</th>
                <th scope="col">Número de Semana</th>
                <th scope="col">ID de Estado</th>
                <th scope="col">Número de Contagios</th>
            </tr>
            </thead>
            <tbody>
            <?php
            while ($row=mysqli_fetch_assoc($Sem1)){?>
                <tr>
                    <th scope="row"><?php echo $row["id_semana"];?></th>
                    <td><?php echo $row["num_semana"];?></td>
                    <td><?php echo $row["ID_Estado"];?></td>
                    <td><?php echo $row["Num_Contagiados"];?></td>
                </tr>
            <?php }?>
            </tbody>
        </table>

        <br>
        <h3 class="text-center">Semana 2</h3>
        <table class="table">
            <thead>
            <tr class="bg-primary">
                <th scope="col">No.</th>
                <th scope="col">Número de Semana</th>
                <th scope="col">ID de Estado</th>
                <th scope="col">Número de Contagios</th>
            </tr>
            </thead>
            <tbody>
            <?php
            while ($row=mysqli_fetch_assoc($Sem2)){?>
                <tr>
                    <th scope="row"><?php echo $row["id_semana"];?></th>
                    <td><?php echo $row["num_semana"];?></td>
                    <td><?php echo $row["ID_Estado"];?></td>
                    <td><?php echo $row["Num_Contagiados"];?></td>
                </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
</body>
</html>