<?php


use models\Usuario;
//Incluimos las clases a utilizar
include "app/models/Connection.php";
include "app/models/Usuario.php";

class usuarioController
{
    public function home(){
        require ('app/views/home.php');
    }
    public function loginView(){
        require 'app/views/login.php';
    }
    public function reUsu(){
        require ('app/views/register.php');
    }
    public function ver(){
        require ('app/views/register.php');
    }
    public function registrar(){
        if(isset($_POST)){
            //Se instancia el objeto de productos para utilizar sus atributos
            $usuario = new \models\Usuario();
            //Accedemos a sus atributos y les damos valores por metodo POST
            $usuario->nombre = $_POST['nombre'];
            $usuario->aP = $_POST['aP'];
            $usuario->aM = $_POST['aM'];
            $usuario->sexo = $_POST['sexo'];
            $usuario->edad=$_POST['edad'];
            $usuario->correo=$_POST['correo'];
            $usuario->pass=$_POST['pass'];
            $usuario->insertar();
            //Imprimimos que la operacion fue realizada correctamente
            echo("<script type='text/javascript'>alert('Registro Exitoso'); </script>");
            require ('app/views/home.php');

        }
        if(!isset($_POST)){
            require('app/views/error.php');
        }
    }
    function login(){
        if(isset($_POST['correo']) && isset($_POST['password']) ){
            //CREAMOS NUEVO OBJETO DE USUARIO
            $user = new \models\Usuario();

            //Recogemos variables
            $correo = $_POST['correo'];
            $password = $_POST['password'];

            //Llama al metodo con parametros
            $verificar = Usuario::logins($correo, $password);

            //Verificar parametros
            if(!$verificar){
                echo "Datos erroneos";
                echo '<br><a href="index.php">Regresar</a>';
            }
            else{
                session_start();
                //Guarda los datos del usuariio logeado
                $_SESSION['usuario'] = $verificar;

                //Guarda el id del usuario logeado
                $_SESSION['idUsuario'] = $verificar->id_usuario;
                echo "Bienvenido: " . $verificar->nombre;
                echo '<br><a href="index.php">ENTRAR</a>';
            }

        }
        else {
            require 'app/views/login.php';

        }

    }
    //funcion para cerrar sesion
    function logout()
    {
        session_start();
        session_destroy();
        require 'app/views/home.php';
    }

}