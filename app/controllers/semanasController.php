<?php
use models\Estados;
//Incluimos las clases a utilizar
include "app/models/Connection.php";
include "app/models/Semanas.php";


class semanasController
{
    public function registrarSem(){
        require ('app/views/semanaRegis.php');
    }
    public function compaSem1(){
        require ('app/views/semanas.php');
    }

    //Funcion para consultar los contagios promedio por semana
    public function contagiosSemanas(){
        $semana = new \models\Semanas();
        $estado = $_POST['estado'];
        $seman = $_POST['semana1'];
        $seman2= $_POST['semana2'];
        $Sem1=$semana->semanaInfo($seman,$estado);
        $Sem2=$semana->semanaInfo2($seman2,$estado);
        require ('app/views/semanasComp.php');
    }
    //Funcion para insertar
    function registrar(){
        //Comprueba que se hayan mandado valores
        if(isset($_POST)){
            //Se instancia el objeto de productos para utilizar sus atributos
            $semana = new \models\Semanas();
            //Accedemos a sus atributos y les damos valores por metodo POST
            $estado=$_POST['estado'];
            $semana->num_sem=$_POST['num_sem'];
            //Imprimimos que la operacion fue realizada correctamente
            $semana->insertar($estado);
        }
    }

}