<?php
use models\Estados;
//Incluimos las clases a utilizar
include "app/models/Connection.php";
include "app/models/Personas.php";


class personasController
{
    /*
     * CONSULTA
     */
    //Funcion para consultar datos
    public function imprimirPersonas(){

        //Imprimimos que la operacion fue realizada correctamente
        $persona = \models\Personas::findAll();
        require ('app/views/personas.php');
    }
    public function rePer(){
        require ('app/views/registrarPersona.php');
    }
    public function eliminar(){
        $id=$_GET['id'];
        $persona = new \models\Personas();
        $persona->eliminar($id);
        echo("<script type='text/javascript'>alert('Se elimino el registro'); </script>");
        require ('app/views/home.php');

    }

    //Funcion para insertar
    function registrar(){
        //Comprueba que se hayan mandado valores
        if(isset($_POST)){
            //Se instancia el objeto de productos para utilizar sus atributos
            $persona = new \models\Personas();
            //Accedemos a sus atributos y les damos valores por metodo POST
            $persona->idE = $_POST['estado'];
            $persona->sexo = $_POST['sexo'];
            $persona->edad = $_POST['edad'];
            $persona->semana=$_POST['semana'];
            $persona->estatus = $_POST['estatus'];
            $persona->insertar();
            echo("<script type='text/javascript'>alert('Registro Exitoso'); </script>");
            require ("app/views/registrarPersona.php");
        }
        //vistaRegistro();
    }

    //Funcion imprimir por estado
    public function imprimirEstado(){
        $estado = $_POST['estado'];
        $promedioE = \models\Personas::allEdadPromedio($estado);
        $sexoM = \models\Personas::sexoM($estado);
        $sexoP = \models\Personas::sexoF($estado);
        require ("app/views/estado.php");
    }
    //Funcion imprimir contagiados por estado
    public function imprimirEstadoC(){
        $estado = $_POST['estado'];
        $promedioE = \models\Personas::allEdadPromedioC($estado);
        $sexoM = \models\Personas::sexoContagiadoM($estado);
        $sexoP = \models\Personas::sexoContagiadoF($estado);
        require ("app/views/estadoContagio.php");
    }

    //Funcion para consultar todas las edades
    public function edadProm(){
        $estado = $_GET['estado'];
        //Imprimimos que la operacion fue realizada correctamente
        echo json_encode(["status"=>"succes edadProm","presonas"=> $persona = \models\Personas::allEdadPromedio($estado)]);
    }

    //Funcion para consultar solo los infectados o no
    public function edadPromContagios(){
        $tipo = $_POST['tipo'];

        //Imprimimos que la operacion fue realizada correctamente
        echo json_encode(["status"=>"succes edadPromContagios","presonas"=> $persona = \models\Personas::edadPromedioC($tipo)]);
    }

}