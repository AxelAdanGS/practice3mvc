<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index</title>
</head>
<body>
    <?php

        //Comprueba que se le pase un controlador y una accion
        if(isset($_GET["controller"]) && isset($_GET["action"])){
            //Asigna el valor ingresado de cada metodo
            $controller = $_GET["controller"]."Controller";
            $action = $_GET["action"];

            //Llama a la clase a utilizar concatenando "controller"
        

            //
            require "app/controllers/$controller.php";
            //require_once("Controller/" .$controller. ".php");

            $objeto = new $controller();

            $objeto->{$action}();
        } else{
            require "app/views/home.php";
        }
    ?>
</body>
</html>